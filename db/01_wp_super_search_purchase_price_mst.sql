-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2020 年 2 月 10 日 03:11
-- サーバのバージョン： 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.26-2+ubuntu16.04.1+deb.sury.org+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wordpress`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `wp_super_search_purchase_price_mst`
--

CREATE TABLE `wp_super_search_purchase_price_mst` (
  `purchase_price_id` varchar(10) COLLATE utf8_unicode_520_ci NOT NULL,
  `shop_id` varchar(2) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `shop_name` varchar(2048) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `item_id` varchar(11) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `group_id` varchar(3) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `name` varchar(2048) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `no_disp` varchar(1) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `priority` int(2) DEFAULT NULL,
  `remark` varchar(2048) COLLATE utf8_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_super_search_purchase_price_mst`
--
ALTER TABLE `wp_super_search_purchase_price_mst`
  ADD PRIMARY KEY (`purchase_price_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
