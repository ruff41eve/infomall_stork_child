-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018 年 9 月 18 日 01:46
-- サーバのバージョン： 5.6.37
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `local`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `wp_super_search_item_info`
--

CREATE TABLE `wp_super_search_item_info` (
  `iid` varchar(10) COLLATE utf8_unicode_520_ci NOT NULL,
  `category_id` varchar(5) COLLATE utf8_unicode_520_ci NOT NULL,
  `item_name` varchar(2048) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `status` varchar(128) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `seller` varchar(128) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `url` varchar(2048) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `price` int(8) DEFAULT NULL,
  `img_url` varchar(2048) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `pay_type` varchar(2048) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `up_time` datetime DEFAULT NULL,
  `no_disp` varchar(1) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `pfg` varchar(5) COLLATE utf8_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_kurasim_device_mst`
--
ALTER TABLE `wp_super_search_item_info`
  ADD PRIMARY KEY (`iid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
