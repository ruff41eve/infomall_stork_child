<?php require_once(ABSPATH.'/wp-load.php');//Wordpress関数を使えるようにする ?>
<?php /* RSSを更新順に出力 */ ?>
<?php echo "<?xml version='1.0' encoding='UTF-8' ?>\n"; ?>
<rss version="2.0">
<channel>
  <title><?php echo bloginfo('name');//サイト名 ?></title>
  <link><?php echo bloginfo('url');//サイトアドレス ?></link>
  <description></description>
  <language>ja</language>
  <?php query_posts("&orderby=modified&posts_per_page=".get_option("posts_per_rss"));//更新日順に並び替え ?>
  <?php while (have_posts()) : the_post(); ?>
  <item>
  <title><?php the_title();//タイトル ?></title>
  <guid isPermaLink="false"></guid>
  <link><?php the_permalink(); ?></link>
  <pubDate><?php the_time('c');//公開日 ?></pubDate>
  <updated><?php the_modified_time('c');//更新日 ?></updated>
  <description><![CDATA[
  <?php the_excerpt();//抜粋 ?>
  ]]></description>
  </item>
  <?php endwhile; ?>
  <?php wp_reset_query(); ?>
</channel>