<?php

	/* DBアクセス用 */
	global $wpdb;
	
	/*************************************************************************
	 * 各処理 ファイル読み込み
	*************************************************************************/

	// 定数定義
	require('constant.php');
	// CSS/Javascript読込
	require('functions_include.php');
	// 共通処理
	require('functions_common.php');
	// HTML構築
	require('functions_make_html.php');
	// ショートコード(共通)
	require('functions_shortcode_common.php');
	// DB接続
	require('functions_db.php');
	
?>