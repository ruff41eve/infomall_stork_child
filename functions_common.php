<?php
	/*************************************************************************
	 * 共通関数
	*************************************************************************/

	/**
	 * Widgetでショートコードを利用可能にする
	 */
	add_filter('widget_text', 'do_shortcode' );

	/**
	 * カテゴリでショートコードを利用可能にする
	 */
	add_filter( 'category_description', 'do_shortcode' );

	/**
	 * html/以下のファイル取得
 	 */
	  function get_html_file($filename) {
		$html = "";
		$lines = file(get_theme_file_path('html/' . $filename . '.html'));
		foreach ($lines as $line_num => $line) {
			$html .= $line;
		}
		return $html;
	}

	/**
	 * リダイレクト処理
	 * SEO対策、URLの一元管理のため、中間ページに遷移させてから外部へ遷移する
	 * GETパラメータ(url)にURL_IDを指定してください。
	 * 例）https://sahara-star.net/seikatsu/redirect/?url=url-sel-rom001
 	*/
	 function my_require(){
		// 固定ページ
		if (is_page()) {
			global $post;
			if ($post->ID === 9999999999) {
				// 商品ID
				$iid = (isset($_GET['iid']) && $_GET['iid'] != '') ? $_GET["iid"] : '';
				$pfg = (isset($_GET['pfg']) && $_GET['pfg'] != '') ? $_GET["pfg"] : '';
				// 検索WORD
				$word = (isset($_GET['word']) && $_GET['word'] != '') ? $_GET["word"] : '';
				if ($iid)
				{
					wp_redirect('https://www.infotop.jp/click.php?aid=382515&iid=' . $iid . '&pfg=' . $pfg);
					exit;
				} 
				else if ($word)
				{
					wp_redirect('https://www.infotop.jp/search?aid=382515&k=' . $word);
					exit;
				}
				// URLが取得できない場合はTOPへ遷移
				wp_safe_redirect( home_url( '/' ) );
				exit;
			}
		}
	}
	add_action('get_header', 'my_require');

	/**
	 * 買取価格更新トリガー処理
	 * 
	 * アクセスされた場合に、買取価格情報を取得するAPIを呼び出し、DBに保持する
	 * 例）https://super-search.jp/?p=9999999997
 	*/
	 function update_price_info_trigger()
	 {
		// 固定ページ
		if (is_page()) {
			global $post;
			if ($post->ID === 9999999997) {
				// ログは wordpress/ 配下に出力される
				//error_log(date( DATE_ATOM ) . "★koko6:" . "\n","3", "test.txt");
				$url = "https://script.google.com/macros/s/AKfycbzYzJx2rXO9mzjpQcrbjb-Tb_28bxLKOt5Z_kVH8U7tcMbV7Kmz/exec?kind=all"; //買取価格情報取得API GAS
				$json = mb_convert_encoding(file_get_contents($url), 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
				$json_arr = json_decode($json,true);
				//error_log(date( DATE_ATOM ) . "★koko7:" . print_r($json_arr, true) . "\n","3", "test.txt");
				// レコードのINSERT or UPDATE
				_update_sql_price_info($json_arr);
				exit;
			}
		}
	}
	add_action('get_header', 'update_price_info_trigger');

	/**
	 * 在庫情報更新トリガー処理
	 * 
	 * アクセスされた場合に、在庫情報を取得するAPIを呼び出し、DBに保持する
	 * 例）https://super-search.jp/?p=9999999998
 	*/
	 function update_stock_info_trigger()
	 {
		// 固定ページ
		if (is_page()) {
			global $post;
			if ($post->ID === 9999999998) {
				// ログは wordpress/ 配下に出力される
				//error_log(date( DATE_ATOM ) . "★koko6:" . "\n","3", "test.txt");
				$url = "https://script.google.com/macros/s/AKfycby2-O96eIp2vvfetHWY-gsuXNelpRGUPVf-IXakUnaMzt-eW1c/exec?kind=all"; //在庫情報取得API GAS
				$json = mb_convert_encoding(file_get_contents($url), 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
				$json_arr = json_decode($json,true);
				// レコードのINSERT or UPDATE
				_update_sql_stock_info($json_arr);
				exit;
			}
		}
	}
	add_action('get_header', 'update_stock_info_trigger');