<?php

	/*************************************************************************
	 * 共通処理
	*************************************************************************/

	/**
	 * SQL実行
	 * @param $_ws_sql 実行するSQL
	 * @return SQL実行結果
	 */
	function _excute_sql($_ws_sql) {
		global $wpdb;
		try { 
			//log_sql($_ws_sql);
			$rows = $wpdb->get_results($_ws_sql);
			return $rows;
		} catch (Exception $e) {
			//log_error($e->errorMessage()); 
		}
	}	

	/**
	 * 買取価格更新
	 * @param $price_info_list 買取価格情報
	 */
	function _update_sql_price_info($price_info_list)
	{
		// 1件ずつ処理
		foreach ($price_info_list as $num => $price_info) {
			$pur_id = $price_info["pur_id"];
			// レコードが存在するか確認する
			$sql = "SELECT * FROM wp_super_search_purchase_price_info WHERE purchase_price_id = '" . $pur_id . "'";
			$sql .= ";";
			$recode = _excute_sql($sql);
			if (count($recode) > 0 )
			{	// 登録済の場合は、UPDATE
				$sql = "";
				$sql .= "UPDATE wp_super_search_purchase_price_info SET ";
				$sql .= "price = '" . $price_info["price"] . "',";
				$sql .= "up_time ='" . $price_info["date"] . "',";
				$sql .= "url = '" . $price_info["item_url"] . "'";
				$sql .= "WHERE purchase_price_id = '". $pur_id . "';";
				_excute_sql($sql);
			} else {
				// 未登録の場合は、INSERT
				$sql = "";
				$sql .= "INSERT INTO wp_super_search_purchase_price_info values (";
				$sql .= "'" . $pur_id . "',";
				$sql .= (is_numeric($price_info["price"]) ? $price_info["price"] : 0) . ",";
				$sql .= "'" . $price_info["date"] . "',";
				$sql .= "'" . $price_info["item_url"] . "'";
				$sql .= ");";
				_excute_sql($sql);
			}
		}
	}

	/**
	 * 買取価格取得
	 * @param $cnt 取得件数
	 */
	function _get_price_info($shop, $item_id, $group_id)
	{
		/*************************************************************************
		 * 対象商品を取得
		*************************************************************************/	
		$sql = "";
		$sql .= "SELECT  ";
		$sql .= "mst.purchase_price_id ";
		$sql .= ",mst.shop_id ";
		$sql .= ",mst.shop_name ";
		$sql .= ",mst.item_id ";
		$sql .= ",mst.group_id ";
		$sql .= ",mst.name ";
		$sql .= ",mst.priority ";
		$sql .= ",mst.remark ";
		$sql .= ",info.price ";
		$sql .= ",info.up_time ";
		$sql .= ",info.url ";
		$sql .= "FROM wp_super_search_purchase_price_mst mst LEFT OUTER JOIN wp_super_search_purchase_price_info info ";
		$sql .= "ON mst.purchase_price_id = info.purchase_price_id  ";
		$sql .= "WHERE TRUE ";
		if ($shop)
		{
			$sql .= "AND mst.shop_id = '" . $shop . "' ";
		}
		if ($item_id)
		{
			$sql .= "AND mst.item_id = '" . $item_id . "' ";
		}
		if ($group_id)
		{
			$sql .= "AND mst.group_id = '" . $group_id . "' ";
		}
		$sql .= "AND mst.no_disp <> '1' ";
		$sql .= "ORDER BY price DESC, priority DESC;";

		return _excute_sql($sql);
	}

	/**
	 * 買取価格情報取得(最新の更新日時取得)
	 * @param $site 
	 */
	function _get_price_latest_up_time($shop, $item_id, $group_id)
	{

		/*************************************************************************
		 * 対象商品を取得
		*************************************************************************/	
		$sql = "";
		$sql .= "SELECT  ";
		$sql .= "MAX(info.up_time) AS latest ";
		$sql .= "FROM wp_super_search_purchase_price_mst mst LEFT OUTER JOIN wp_super_search_purchase_price_info info ";
		$sql .= "ON mst.purchase_price_id = info.purchase_price_id  ";
		$sql .= "WHERE TRUE ";
		if ($shop)
		{
			$sql .= "AND mst.shop_id = '" . $shop . "' ";
		}
		if ($item_id)
		{
			$sql .= "AND mst.item_id = '" . $item_id . "' ";
		}
		if ($group_id)
		{
			$sql .= "AND mst.group_id = '" . $group_id . "' ";
		}
		$sql .= "AND mst.no_disp <> '1';";
		return _excute_sql($sql);
	}

	/**
	 * 商品マスタ取得(単品)
	 * @param $_ws_iid iid商品ID
	 */
	function _get_item_by_iid($_ws_iid) {
		$sql = "";
		$sql .= "SELECT * FROM wp_super_search_item_info ";
		$sql .= "WHERE no_disp!='1' ";
		$sql .= "and iid='" . $_ws_iid . "';";   
		return _excute_sql($sql);
	}