<?php
	/*************************************************************************
	 * CSS/Javascript読込
	*************************************************************************/

	/**
	 * スタイルシート読込
	 */
	function theme_enqueue_styles() {
		// 子テーマのstyle.cssを読み込む
		wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css?' . filemtime(get_theme_file_path('style.css')));

		// 共通スタイル
		wp_enqueue_style('common-style', get_stylesheet_directory_uri() . '/css/infomall_common.css?' . filemtime(get_theme_file_path('css/infomall_common.css'))); 

	}
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' ); 

	/**
	 * 独自JavaScript読込
	 */
	function my_scripts() {
		// 共通Javascript
		wp_enqueue_script('infomall_common',  get_stylesheet_directory_uri() . '/js/seikatsu.js?' . filemtime(get_theme_file_path('js/infomall_common.js')), array(), '', true);

	}
	add_action( 'wp_enqueue_scripts', 'my_scripts');
