<?php
	/*************************************************************************
	 * HTML構築
	*************************************************************************/

	/**
	 * 在庫情報取得 v1 HTML構築
	 * 例) mock_stock_info.html
	 * 
	 * @param $item_list 
	 * @param $site 
	 * @param $shop 
	 * @param $item 
	 * @param $latest_up_time 
	 * 
 	*/
	 function _build_stock_info_v1($item_list, $site, $shop, $item, $latest_up_time)
	 {

		// 最新の日時を取得
		$html = "";
		$html .= '<div class="Ar_stock_list">';
		$html .= '<div class="Ar_stock_title"><span class="Ar_stock_site_name">'. $site .'</span><span class="Ar_stock_item_name">' . $item . '</span></div>';
		$html .= '<div class="Ar_stock_up_time Ar_sp_only">更新日時：' . $latest_up_time . '</div>';
		$html .= '<table>';
		$html .= '<tbody>';
		$html .= '<tr class="Ar_stock_header">';
		$html .= '<th class="Ar_stock_shop"><span>販売店舗<span></th>';
		$html .= '<th class="Ar_stock_info"><span>在庫情報</span></th>';
		$html .= '<th class="Ar_stock_up_time Ar_pc_only"><span>更新日時</span></th>';
		$html .= '<th class="Ar_stock_remark Ar_pc_only"><span>備考</span></th>';
		$html .= '</tr>';
		foreach ($item_list as $key => $item)
		{
			// 在庫情報の付与class
			$stock_class = '';
			switch ($item->stock_info)
			{
				case '在庫あり';
					$stock_class = 'Ar_in_stock';
					break;
				case '在庫なし';
					$stock_class = 'Ar_out_stock';
					break;
				default:
					$stock_class = 'Ar_none_stock';
					break;
			}
			$html .= '<tr class="Ar_stock_contens">';
			$html .= '<td class="Ar_stock_shop"><a target="_blank" href="' . $item->lp_url . '">' . $item->shop_name . '</a></td>';
			$html .= '<td class="Ar_stock_info ' . $stock_class . '"><span>' . $item->stock_info . '</span></td>';
			$html .= '<td class="Ar_stock_up_time Ar_pc_only"><span>' . $item->up_time . '</span></td>';
			$html .= '<td class="Ar_stock_remark Ar_pc_only"><span>' . $item->remark . '</span></td>';
			$html .= '</tr>';
		}
		$html .= '</tbody>';
		$html .= '</table>';
		$html .= '</div>';
		return $html;
	 }

	 /**
	 * 買取価格情報取得 v1 HTML構築
	 * 
	 * @param $item_list 
	 * @param $shop 
	 * @param $item_id 
	 * @param $group_id 
	 * @param $latest_up_time 
	 * 
 	*/
	 function _build_price_info_v1($item_list, $shop, $item_id, $group_id, $latest_up_time)
	 {

		// 最新の日時を取得
		$html = "";
		$html .= '<div class="Ar_price_list">';
		$html .= '<div class="Ar_price_up_time Ar_sp_only">更新日時：' . $latest_up_time . '</div>';
		$html .= '<table>';
		$html .= '<tbody>';
		$html .= '<tr class="Ar_price_header">';
		$html .= '<th class="Ar_price_shop"><span>ショップ名<span></th>';
		$html .= '<th class="Ar_price_info"><span>買取価格</span></th>';
		$html .= '<th class="Ar_price_up_time Ar_pc_only"><span>更新日時</span></th>';
		$html .= '<th class="Ar_price_remark Ar_pc_only"><span>備考</span></th>';
		$html .= '</tr>';
		foreach ($item_list as $key => $item)
		{
			$html .= '<tr class="Ar_price_contens">';
			$html .= '<td class="Ar_price_shop"><a target="_blank" href="' . $item->url . '">' . $item->shop_name . '</a></td>';
			$html .= '<td class="Ar_price_info"><span>' . number_format($item->price) . '円</span></td>';
			$html .= '<td class="Ar_price_up_time Ar_pc_only"><span>' . $item->up_time . '</span></td>';
			$html .= '<td class="Ar_price_remark Ar_pc_only"><span>' . $item->remark . '</span></td>';
			$html .= '</tr>';
		}
		$html .= '</tbody>';
		$html .= '</table>';
		$html .= '</div>';
		return $html;
	 }

 	/**
	 * 商品詳細 メインコンテンツ
	 * @param $db_list アイテム情報
 	*/
	function _get_item_main_html($item)
	{
		$html = '';
		$html .= '<h2>' . $item->item_name . '</h2>';
		$html .= '<div id="Ar_plan_list">';
		$html .= '<div id="Ar_search_item_list">';
		$html .= '<div class="Ar_search_item">';
		$html .= '<div class="Ar_search_item_detail">';
		$html .= '<div class="Ar_search_item_career">';
		$html .= '<div class="Ar_search_item_plan_img">';
		$html .= '<a target="_blank" href="/redirect/?iid=' . $item->iid . '&pfg=' . $item->pfg . '">';
		$html .= '<img src="'. $item->img_url .'" alt="' . $item->item_name . '">';
		$html .= '</a>';
		$html .= '</div>';
		$html .= '<div class="Ar_search_item_botton">';
		$html .= '<div class="Ar_search_item_botton_body">';
		// 個別実装
		if ($item->iid == '35745')
		{
			$html .= '<a href="http://cf101.chu.jp/takeuchi.htm" target="_blank" class="Ac_search_item_botton">';
		} else {
			$html .= '<a href="/redirect/?iid=' . $item->iid . '&pfg=' . $item->pfg . '" target="_blank" class="Ac_search_item_botton">';
		}
		$html .= '<div class="Ar_search_item_botton_text">';
		$html .= '<span class="Ar_search_item_botton_main">詳細ページへ</span>';
		$html .= '</div>';
		$html .= '</a>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div class="Ar_search_item_plan">';
		$html .= '<dl class="clearfix">';
		$html .= '<dt class="Ar_search_item_price">販売価格</dt>';
		$html .= '<dd class="Ar_search_item_price">';
		$html .= '<span class="Ar_search_item_price_num">' . number_format(ceil($item->price)) . '</span>';
		$html .= '<span class="Ar_search_item_price_yen">円</span>';
		$html .= '</dd>';
		$html .= '<dt class="Ar_search_item_seller">販売者</dt>';
		// 個別実装
		if ($item->iid == '35745')
		{
			$html .= '<dd class="Ar_search_item_seller"><a target="_blank" rel="sponsored" href="http://cf101.chu.jp/takeuchi.htm">' . $item->seller . '</a></dd>';
		} else {
			$html .= '<dd class="Ar_search_item_seller"><a target="_blank" rel="sponsored" href="/redirect/?word=' . $item->seller . '">' . $item->seller . '</a></dd>';
		}
		$html .= '<dt class="Ar_search_item_pay_type">支払方法</dt>';
		// 支払方法
		$_wa_pay_type = explode('|', $item->pay_type);
		$_ws_pay_type = '';
		foreach ($_wa_pay_type as $key => $pay_type)
		{
			$_ws_pay_type .= '<span>' . $pay_type . '</span>';;
		}
		$html .= '<dd class="Ar_search_item_pay_type">' . str_replace('<br><br>', '<br>', str_replace('<br><br>', '<br>', $_ws_pay_type)) . '</dd>';
		$html .= '<dt class="Ar_search_item_detail">商品説明</dt>';
		//$html .= '<!-- 連続改行を1つへする処理を実装 -->';
		$html .= '<dd class="Ar_search_item_detail">' . $item->description . '</dd>';
		$html .= '<dt class="Ar_search_item_update">更新日時</dt>';
		$html .= '<dd class="Ar_search_item_update">' . date('Y年m月d日 H:i', strtotime($item->up_time)) . '</dd>';
		$html .= '</dl>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div id="Ar_floating_fooder_area" class="Ar_floating_are Ar_sp_only">';
		$html .= '<div class="Ar_floating_list_p2">';
		$html .= '<div class="Ar_floating_list_p2_body">';
		$html .= '<div class="Ar_floating_botton">';
		$html .= '<a href="/redirect/?iid=' . $item->iid . '&pfg=' . $item->pfg . '" target="_blank" class="Ac_floating_botton">';
		$html .= '<div class="Ar_floating_botton_text">';
		$html .= '<span class="Ar_floating_header_botton_main">商品詳細ページへ</span>';
		$html .= '</div>';
		$html .= '</a>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';
		return $html;
	}

 	/**
	 * 商品詳細 下部コンテンツ
	 * @param $db_list アイテム情報
 	*/
	 function _get_item_bottom_html($item)
	 {
		 $html = '';
		 $html .= '<h2>「' . $item->item_name . '」の口コミ投稿フォーム</h2>';
		 $html .= '<small><p>転売・せどりサーチでは、<strong>完全ノーリスク無在庫転売ツール利用者の口コミ</strong>を募集しています。<br>';
		 $html .= 'いただいた口コミは内容を確認し、場合によっては完全ノーリスク無在庫転売ツール利用者の評判として、『' . $item->item_name . '』の申し込み・購入を検討している方のために活用させていただきます。</p>';
		 $html .= '<p>尚、口コミは完全ノーリスク無在庫転売ツールに限らず、その他の情報商材でも募集しております。<br>';
		 $html .= '下記の投稿フォームよりお寄せください。</p></small>';
		 $html .= '<div class="btn-wrap aligncenter rich_green">';
		 $html .= '<a href="https://ws.formzu.net/fgen/S83349984/" rel="noopener nofollow" target="_blank">口コミ投稿フォームへ！</a>';
		 $html .= '</div>';
		 $html .= '<h2>「' . $item->item_name . '」に関する保証の否認及び免責</h2>';
		 $html .= '<small>当ページでご紹介している『' . $item->item_name . '』は、当サイトが作成・監修した情報商材ではございません。<br>';
		 $html .= 'そのため、情報の内容の完全性や正確性、有効性等を保証しておりません。<br>';
		 $html .= 'これらの情報をご利用になったこと、またはご利用になれなかったことにより生じたいかなる損害についても責任を負いません。<br>';
		 $html .= '尚、『' . $item->item_name . '』の内容についての詳細や質問に関しましては、直接、<a href="/redirect/?iid=' . $item->iid . '&pfg=' . $item->pfg . '" target="_blank">ツール販売会社様</a>にご相談ください。</small>';
 		 return $html;
	 }