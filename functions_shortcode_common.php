<?php
	/*************************************************************************
	 * ショートコード
	*************************************************************************/
	/**
 	* コメントアウト用 ショートコード
 	*/
	 function ignore_shortcode($atts, $content = null) {

		// ショートコードの中にショートコードがあっても実行できるようにする
		$content = do_shortcode(shortcode_unautop($content)); 
		return null;
	} 
	add_shortcode('ignore', 'ignore_shortcode');

	/**
 	* タイマー表示(指定期間以降) ショートコード
 	*/
	function timer_disp_after_shortcode($atts, $content = null) {

		// ショートコードの中にショートコードがあっても実行できるようにする
		$content = do_shortcode(shortcode_unautop($content));
		$after = (isset($atts['after']) && $atts['after'] != '') ? $atts['after'] : '';
		if ($after) {
			date_default_timezone_set('Asia/Tokyo');
			$now = date("YmdHi");
			if ((int)$now >= (int)$after) {
				return $content;
			}
		}
		return null;
	} 
	add_shortcode('timer_disp_after', 'timer_disp_after_shortcode');

	/**
 	* タイマー表示(指定期間以前) ショートコード
 	*/
	function timer_disp_before_shortcode($atts, $content = null) {

		// ショートコードの中にショートコードがあっても実行できるようにする
		$content = do_shortcode(shortcode_unautop($content));
		$before = (isset($atts['before']) && $atts['before'] != '') ? $atts['before'] : '';
		if ($before) {
			date_default_timezone_set('Asia/Tokyo');
			$now = date("YmdHi");
			if ((int)$now < (int)$before) {
				return $content;
			}
		}
		return null;
	} 
	add_shortcode('timer_disp_before', 'timer_disp_before_shortcode');

	/**
 	* タイマー表示(指定期間) ショートコード
 	*/
	function timer_disp_period_shortcode($atts, $content = null) {

		// ショートコードの中にショートコードがあっても実行できるようにする
		$content = do_shortcode(shortcode_unautop($content));
		$start = (isset($atts['start']) && $atts['start'] != '') ? $atts['start'] : '';
		$before = (isset($atts['end']) && $atts['end'] != '') ? $atts['end'] : '';
		if ($start && $before) {
			date_default_timezone_set('Asia/Tokyo');
			$now = date("YmdHi");
			if ((int)$now >= (int)$start && (int)$now < (int)$before) {
				return $content;
			}
		}
		return null;
	} 
	add_shortcode('timer_disp_period', 'timer_disp_period_shortcode');

	/**
	 * SP用のフローティングボタン
	 * オレンジ
 	*/
	function floating_button($atts, $content = null) {
		$html = "";
		$html .= '<div id="Ar_floating_fooder_area" class="Ar_floating_are Ar_sp_only" style=""><div class="Ar_floating_list_p2"><div class="Ar_floating_list_p2_body"><div class="Ar_floating_botton"><a href="';
		$html .= $atts['url'];
		$html .= '" target="_blank" class="Ac_floating_botton"><div class="Ar_floating_botton_text"><span class="Ar_floating_header_botton_main">';
		$html .= $atts['title'];
		$html .= '</span></div></a></div></div></div></div>';
		return $html;
	}
	add_shortcode('sp_button', 'floating_button');

	/**
	 * ファイル読み込み
	 * 例)
	 * [include file='folder/test.html']
 	*/
	function includeFile($atts) {
 		if (isset($atts['file'])) {
			$file = $atts['file'];
			return file_get_contents(dirname(__FILE__) . '/' . $file);
		} 
	}
	add_shortcode('include', 'includeFile');

	/**
 	 * 在庫情報取得 v1
	 * $atts['site'] サイト名
	 * $atts['shop'] ショップ名
	 * $atts['item'] 商品名
	 *
	 *  例)
	 * [stock_info_v1 site='楽天市場' shop='楽天ブックス' item='新モデルNintendoSwitch ネオン']
	 * [stock_info_v1 site='楽天市場' item='新モデルNintendoSwitch ネオン']
 	*/
	 function stock_info_v1_shortcode($atts, $content = null) {

		/*************************************************************************
		 * 対象商品を取得
		*************************************************************************/
		if (!isset($atts['site']) && !isset($atts['shop']) && !isset($atts['item'])) {
			return null;
		}
		$site = isset($atts['site']) ? $atts['site'] : '';
		$shop = isset($atts['shop']) ? $atts['shop'] : '';
		$item = isset($atts['item']) ? $atts['item'] : '';

		$item_list = _get_stock_info($site, $shop, $item);
		//error_log(date( DATE_ATOM ) . "★koko6:" .  print_r($item_list, true) . "\n","3", "test.txt");
		$latest_up_time = _get_stock_latest_up_time($site, $shop, $item);
		//error_log(date( DATE_ATOM ) . "★koko6:" .  print_r($latest_up_time, true) . "\n","3", "test.txt");

		/*************************************************************************
		 * HTMLを構築
		*************************************************************************/
		return _build_stock_info_v1($item_list,$site, $shop, $item, $latest_up_time[0]->latest);
	} 
	add_shortcode('stock_info_v1', 'stock_info_v1_shortcode');

	/**
 	 * 買取価格取得 v1
	 * $atts['shop'] サイト名
	 * $atts['item_id'] アイテムID
	 * $atts['group_id'] グループID
	 *
	 *  例)
	 * [price_info_v1 shop='50' item_id='ITEM_01_001' group_id='SWT']
 	*/
	 function price_price_v1_shortcode($atts, $content = null) {

		/*************************************************************************
		 * 対象商品を取得
		*************************************************************************/
		if (!isset($atts['shop']) && !isset($atts['item_id']) && !isset($atts['group_id']))
		{
			return null;
		}
		$shop = isset($atts['shop']) ? $atts['shop'] : '';
		$item_id = isset($atts['item_id']) ? $atts['item_id'] : '';
		$group_id = isset($atts['group_id']) ? $atts['group_id'] : '';

		$item_list = _get_price_info($shop, $item_id, $group_id);
		$latest_up_time = _get_price_latest_up_time($shop, $item_id, $group_id);
		//error_log(date( DATE_ATOM ) . "★koko6:" .  print_r($latest_up_time, true) . "\n","3", "test.txt");

		/*************************************************************************
		 * HTMLを構築
		*************************************************************************/
		return _build_price_info_v1($item_list, $shop, $item_id, $group_id, $latest_up_time[0]->latest);
	} 
	add_shortcode('price_info_v1', 'price_price_v1_shortcode');

	/**
	 * [item_top]ショートコード
	 * 商品ページ上部コンテンツ
 	 */
	  function item_top_shortcode($atts, $content = null) {
		$html = '';
		// 現状何もなし。
		return $html;
	} 
	add_shortcode('item_top', 'item_top_shortcode');

	/**
	 * [item_main]ショートコード
	 * 商品ページメインコンテンツ
 	*/
	function item_main_shortcode($atts, $content = null) {
		$html = '';

		// 商品情報を取得する
		$item = _get_item_by_iid($atts['id']);
		if ($item && $item[0])
		{
			$html = '';
			$html .= _get_item_main_html($item[0]);
		}
		return $html;
	} 
	add_shortcode('item_main', 'item_main_shortcode');

	/**
	 * [item_bottom]ショートコード
	 * 商品ページ下部コンテンツ
 	 */
  	function item_bottom_shortcode($atts, $content = null) {
		$html = '';

		// 商品情報を取得する
		$item = _get_item_by_iid($atts['id']);
		if ($item && $item[0])
		{
			$html = '';
			$html .= _get_item_bottom_html($item[0]);
		}
		return $html;
	} 
	add_shortcode('item_bottom', 'item_bottom_shortcode');