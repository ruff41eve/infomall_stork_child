/*************************************************************************
 * document.ready
 *************************************************************************/
$(document).ready(function() {
	
	/*************************************************************************
	 * アクセスランキングパーツ
	 *************************************************************************/
	var s_word_view = 'ビュー';
	var o_popularParts = $('.wpp-views');
	o_popularParts.each(function(i, element) {
		var s_cnt = $(element).html();
		s_cnt = s_cnt.replace(s_word_view, '').replace(',','');
		var i_cnt = parseInt(s_cnt, 10) * 3;
		var s_result = String(i_cnt).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,' );
		$(element).html(s_result + s_word_view);
	});

	/*************************************************************************
	 * フローティングエリア（ヘッダー)
	 *************************************************************************/
	var o_header_floating = jQuery('#Ar_floating_header_area');
	if (o_header_floating) {
		o_header_floating.hide();
		jQuery(window).scroll(function () {
			// 表示制御
			if (jQuery(this).scrollTop() > 500) {
				// スクロール位置にて表示/非表示
				o_header_floating.fadeIn();
			} else {
				o_header_floating.fadeOut();
			}
		});
	}
	/*************************************************************************
	 * フローティングエリア（フッター)
	 *************************************************************************/
	var o_footer_floating = jQuery('#Ar_floating_fooder_area');
	if (o_footer_floating) {
		o_footer_floating.hide();
		jQuery(window).scroll(function () {
			// 表示制御
			if (jQuery(this).scrollTop() > 200) {
				// スクロール位置にて表示/非表示
				o_footer_floating.fadeIn();
			} else {
				o_footer_floating.fadeOut();
			}
		});
	}
});